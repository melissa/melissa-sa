/******************************************************************
*                            Melissa                              *
*-----------------------------------------------------------------*
*   COPYRIGHT (C) 2017  by INRIA and EDF. ALL RIGHTS RESERVED.    *
*                                                                 *
* This source is covered by the BSD 3-Clause License.             *
* Refer to the  LICENSE file for further information.             *
*                                                                 *
*-----------------------------------------------------------------*
*  Original Contributors:                                         *
*    Theophile Terraz,                                            *
*    Bruno Raffin,                                                *
*    Alejandro Ribes,                                             *
*    Bertrand Iooss,                                              *
******************************************************************/

#include <melissa/stats/mean.h>
#include <melissa/utils.h>

#include <stdio.h>
#include <string.h>


/**
 * This function initializes a mean structure.
 *
 * @param[in,out] *mean
 * the mean structure to initialize
 *
 * @param[in] vect_size
 * size of the mean vector
 *
 */

void init_mean (mean_t    *mean,
                const int  vect_size)
{
    mean->mean = melissa_calloc (vect_size, sizeof(double));
    mean->increment = 0;
}

/**
 * This function updates the incremental mean.
 *
 * @param[in,out] *mean
 * input: previously computed iterative mean,
 * output: updated mean
 *
 * @param[in] in_vect[]
 * input vector of double values
 *
 * @param[in] vect_size
 * size of the input vectors
 *
 */

void increment_mean (mean_t    *mean,
                     double     in_vect[],
                     const int  vect_size)
{
    int     i;
    double temp;

    mean->increment += 1;
    for (i=0; i<vect_size; i++)
    {
        temp = mean->mean[i];
        // mean = temp + in_vect/increment
        mean->mean[i] = temp + (in_vect[i] - temp)/mean->increment;
    }
}

/**
 * This function agregates the partial means from all process on precess 0.
 *
 * @param[in,out] *mean[]
 * input: partial mean,
 * output: global mean on process 0
 *
 * @param[in] vect_size
 * size of the input vector
 *
 * @param[in] rank
 * process rank in "comm"
 *
 * @param[in] comm_size
 * nomber of process in "comm"
 *
 * @param[in] comm
 * MPI communicator
 *
 */

void update_global_mean (mean_t    *mean,
                         const int  vect_size,
                         const int  rank,
                         const int  comm_size,
                         MPI_Comm   comm)
{
    double     *global_mean = NULL;
    double     *global_mean_ptr = NULL;
    double     *mean_ptr = NULL;
    double      delta;
    int         temp_inc;
    int         i, j;
    MPI_Status  status;

    if (rank == 0)
    {
        global_mean = melissa_malloc (vect_size * sizeof(double));
        memcpy (global_mean, mean->mean, vect_size * sizeof(double));

        for (i=1; i<comm_size; i++)
        {
            MPI_Recv (&temp_inc, 1, MPI_INT, i, i, comm, &status);
            MPI_Recv (mean->mean, vect_size, MPI_DOUBLE, i, comm_size+i, comm, &status);

            mean_ptr = mean->mean;
            global_mean_ptr = global_mean;

            for (j=0; j<vect_size; j++, mean_ptr++, global_mean_ptr++)
            {
                delta = (*global_mean_ptr - *mean_ptr);
                *global_mean_ptr = *mean_ptr + mean->increment * delta / (mean->increment + temp_inc);
            }
            mean->increment += temp_inc;
        }
        memcpy (mean->mean, global_mean, vect_size * sizeof(double));
        melissa_free (global_mean);
    }
    else
    {
        MPI_Send (&(mean->increment), 1, MPI_INT, 0, rank, comm);
        MPI_Send (mean->mean, vect_size, MPI_DOUBLE, 0, comm_size+rank, comm);
    }
}

/**
 * This function writes an array of mean structures on disc
 *
 * @param[in] *means
 * mean structures to save, size nb_time_steps
 *
 * @param[in] vect_size
 * size of double vectors
 *
 * @param[in] nb_time_steps
 * number of time_steps of the study
 *
 * @param[in] f
 * file descriptor
 *
 */

void save_mean(mean_t *means,
               int     vect_size,
               int     nb_time_steps,
               FILE*   f)
{
    int i;
    for (i=0; i<nb_time_steps; i++)
    {
        fwrite(means[i].mean, sizeof(double), vect_size, f);
        fwrite(&means[i].increment, sizeof(int), 1, f);
    }
}

/**
 * This function reads an array of mean structures on disc
 *
 * @param[in] *means
 * mean structures to read, size nb_time_steps
 *
 * @param[in] vect_size
 * size of double vectors
 *
 * @param[in] nb_time_steps
 * number of time_steps of the study
 *
 * @param[in] f
 * file descriptor
 *
 */

void read_mean(mean_t *means,
               int     vect_size,
               int     nb_time_steps,
               FILE*   f)
{
    int i;
    for (i=0; i<nb_time_steps; i++)
    {
        fread(means[i].mean, sizeof(double), vect_size, f);
        fread(&means[i].increment, sizeof(int), 1, f);
    }
}

/**
 * This function frees a mean structure.
 *
 * @param[in] *mean
 * the mean structure to free
 *
 */

void free_mean (mean_t *mean)
{
    melissa_free (mean->mean);
}

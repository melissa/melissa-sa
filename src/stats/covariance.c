/******************************************************************
*                            Melissa                              *
*-----------------------------------------------------------------*
*   COPYRIGHT (C) 2017  by INRIA and EDF. ALL RIGHTS RESERVED.    *
*                                                                 *
* This source is covered by the BSD 3-Clause License.             *
* Refer to the  LICENSE file for further information.             *
*                                                                 *
*-----------------------------------------------------------------*
*  Original Contributors:                                         *
*    Theophile Terraz,                                            *
*    Bruno Raffin,                                                *
*    Alejandro Ribes,                                             *
*    Bertrand Iooss,                                              *
******************************************************************/

#include <melissa/stats/covariance.h>
#include <melissa/stats/mean.h>
#include <melissa/stats/variance.h>
#include <melissa/utils.h>

#include <stdio.h>

/**
 * This function initializes a covariance structure.
 *
 * @param[in,out] *covariance
 * the covariance structure to initialize
 *
 * @param[in] vect_size
 * size of the covariance vector
 *
 */

void init_covariance (covariance_t *covariance,
                      const int     vect_size)
{
    covariance->covariance = melissa_calloc (vect_size, sizeof(double));
    init_mean (&covariance->mean1, vect_size);
    init_mean (&covariance->mean2, vect_size);
    covariance->increment = 0;
}

/**
 * This function updates the incremental covariance.
 *
 * @param[in,out] *covariance
 * input: previously computed covariance,
 * output: incremented covariance
 *
 * @param[in] in_vect1[]
 * first input vector of double values
 *
 * @param[in] in_vect2[]
 * second input vector of double values
 *
 * @param[in] vect_size
 * size of the input vectors
 *
 */

void increment_covariance (covariance_t *covariance,
                           double        in_vect1[],
                           double        in_vect2[],
                           const int     vect_size)
{
    int     i;
    double  incr = 0;

    covariance->increment += 1;
    incr = (double)covariance->increment;
    increment_mean(&(covariance->mean1), in_vect1, vect_size);
    if (covariance->increment > 1)
    {
        for (i=0; i<vect_size; i++)
        {
            covariance->covariance[i] *= (incr - 2);
            covariance->covariance[i] += (in_vect1[i] - covariance->mean1.mean[i]) * (in_vect2[i] - covariance->mean2.mean[i]);
            covariance->covariance[i] /= (incr - 1);
        }
    }
    increment_mean(&(covariance->mean2), in_vect2, vect_size);
}


/**
 * This function writes an array of covariances structures on disc
 *
 * @param[in] *covars
 * covariance structures to save, size nb_time_steps
 *
 * @param[in] vect_size
 * size of double vectors
 *
 * @param[in] nb_time_steps
 * number of time_steps of the study
 *
 * @param[in] f
 * file descriptor
 *
 */

void save_covariance(covariance_t *covars,
                     int           vect_size,
                     int           nb_time_steps,
                     FILE*         f)
{
    int i;
    for (i=0; i<nb_time_steps; i++)
    {
        fwrite(covars[i].covariance, sizeof(double), vect_size, f);
        save_mean (&covars[i].mean1, vect_size, 1, f);
        save_mean (&covars[i].mean2, vect_size, 1, f);
        fwrite(&covars[i].increment, sizeof(int), 1, f);
    }
}

/**
 * This function reads an array of covariances structures on disc
 *
 * @param[in] *covars
 * covariance structures to read, size nb_time_steps
 *
 * @param[in] vect_size
 * size of double vectors
 *
 * @param[in] nb_time_steps
 * number of time_steps of the study
 *
 * @param[in] f
 * file descriptor
 *
 */

void read_covariance(covariance_t *covars,
                     int           vect_size,
                     int           nb_time_steps,
                     FILE*         f)
{
    int i;
    for (i=0; i<nb_time_steps; i++)
    {
        fread(covars[i].covariance, sizeof(double), vect_size, f);
        read_mean (&covars[i].mean1, vect_size, 1, f);
        read_mean (&covars[i].mean2, vect_size, 1, f);
        fread(&covars[i].increment, sizeof(int), 1, f);
    }
}

/**
 * This function frees a covariance structure.
 *
 * @param[in] *covariance
 * the covariance structure to free
 *
 */

void free_covariance (covariance_t *covariance)
{
    melissa_free (covariance->covariance);
    free_mean (&covariance->mean1);
    free_mean (&covariance->mean2);
}

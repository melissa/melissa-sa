# Examples

This directory contains various simulation examples for running Melissa sensitivity analysis:
* [heat-pde](heat-pde): a basic Melissa example for a simple parallelized heat equation solver,
* [Code\_Saturne](Code_Saturne): an advanced example using the [Code\_Saturne](https://www.code-saturne.org/) CFD solver.





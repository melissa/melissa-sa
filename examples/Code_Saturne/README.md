# Melissa Code\_Saturne Example

The example has been tested with Code\_Saturne version 7.0.0 but we expect it to work with Code\_Saturne series 6 as well.


## Building Code\_Saturne with Melissa support

Please follow the official installation instructions (e.g., in the [Code\_Saturne version 6.0.0 installation guide](http://code-saturne.org/cms/sites/default/files/docs/6.0/install.pdf)). To enable Melissa support, locate your Melissa installation and add the following argument on the `configure` command line:
```sh
--with-melissa=/path/to/melissa/prefix
```

The Code\_Saturne source code comes a Python script called `install_saturne.py`. This script cannot be used for building Code\_Saturne because it does not support builds with Melissa support enabled.

Note that the Code\_Saturne tarballs on github.com are not identical to the tarballs on [code-saturne.org](https://code-saturne.org/): the latter contain documentation and do not require autotools to be installed.


## Running the Code\_Saturne Example

Before the example can be run, ensure Melissa and Code\_Saturne are found by your shell. If you installed with Spack, try
```sh
spack load code-saturne
```
After a manual installation, run
```sh
source /path/to/melissa/bin/melissa-setup-env.sh
```
Check if Melissa and Code\_Saturne are found by  your shell by executing the following commands in a shell:
```sh
melissa-config --version
code_saturne info --version
```
For convenience, create a symbolic link to the directory containing the example code:
```sh
ln -sv "$(melissa-config --prefix)/share/melissa/examples/Code_Saturne"
```
Code\_Saturne needs a setup phase before launching the actual simulation. In the setup (or initialization phase), a single process on the local machine set ups the directories and Code\_Saturne input files. The setup phase be achieved by means of the the `--with-simulation-setup` flag.

The command below runs the Code\_Saturne example in its default configuration and starts a number Code\_Saturne instances with OpenMPI _at the same time_:
```sh
melissa-launcher \
    --with-simulation-setup
    openmpi \
    Code_Saturne/options.py" \
    Code_Saturne/run-code-saturne.py"
```

#!/usr/bin/python3

# Copyright (c) 2020-2021, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from typing import cast, Dict, Generic, List, Optional, TypeVar, Union

from .options import Options

JobT = TypeVar('JobT')


class Scheduler(Generic[JobT]):
    def sanity_check(self, options: Options) -> List[str]:
        errors = self.sanity_check_impl(options)
        return errors

    def submit_job(self, command: List[str], *args, **kwargs) -> JobT:
        return self.submit_heterogeneous_job([command], *args, **kwargs)

    def submit_heterogeneous_job(
        self,
        commands: List[List[str]],
        options: Options,
        name: str,
        env: Dict[str, Union[None, str]] = {}
    ) -> JobT:
        if commands == []:
            raise ValueError("expected at least one command, got none")

        # allow None as dictionary value so that callers can pass, for example,
        #   env = { "PYTHONPATH": os.getenv("PYTHONPATH") }
        # without having to check if the environment variable was set
        env_clean = dict(
            [(k, cast(str, env[k])) for k in env.keys() if env[k] is not None]
        )

        job = self.submit_heterogeneous_job_impl(
            commands, env_clean, name, options
        )

        return job

    def cancel_jobs(self, jobs: List[JobT]) -> None:
        if jobs == []:
            return

        self.cancel_jobs_impl(jobs)

    def update_jobs(self, jobs: List[JobT]) -> None:
        if jobs == []:
            return

        self.update_jobs_impl(jobs)

    def sanity_check_impl(self, options: Options) -> List[str]:
        return []

    def submit_heterogeneous_job_impl(
        self, command: List[List[str]], environment: Dict[str, str], name: str,
        options: Options
    ) -> JobT:
        raise NotImplementedError(
            "Scheduler.submit_heterogeneous_job_impl not implemented"
        )

    def cancel_jobs_impl(self, jobs: List[JobT]) -> None:
        raise NotImplementedError("Scheduler.cancel_jobs_impl not implemented")

    def update_jobs_impl(self, jobs: List[JobT]) -> None:
        raise NotImplementedError("Scheduler.update_jobs_impl not implemented")

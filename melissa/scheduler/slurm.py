#!/usr/bin/python3

# Copyright (c) 2020-2021, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import os
import re
import shutil
import subprocess
import sys
from typing import Dict, List
import unittest
import warnings

from .. import config
from .job import Job, State
from .options import Options
from .scheduler import Scheduler


class SlurmJob(Job):
    def __init__(self, job_id: int) -> None:
        super(SlurmJob, self).__init__()
        self.job_id_ = job_id
        self.state_ = State.WAITING

    def id(self) -> int:
        return self.job_id_

    def state(self) -> State:
        return self.state_

    def __repr__(self) -> str:
        r = "SlurmJob(id={:d},state={:s})".format(self.id(), str(self.state()))
        return r

    def set_state(self, new_state: State) -> None:
        self.state_ = new_state


class Slurm(Scheduler[SlurmJob]):
    def __init__(self, testing: bool = False) -> None:
        sbatch_job_id_regexp = "(\d+)"
        self.sbatch_job_id_pattern = re.compile(sbatch_job_id_regexp, re.ASCII)
        sacct_line_regexp = "(\d+)([+]0[.]batch)?[|](\w+)"
        self.sacct_line_pattern = re.compile(sacct_line_regexp, re.ASCII)

        # check Slurm version
        if not testing:
            srun = subprocess.run(
                ["srun", "--version"],
                stdin=subprocess.DEVNULL,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                universal_newlines=True,
                check=True
            )

            output_regexp = "\Aslurm (\d+)[.](\d+)[.](\d+)"
            match = re.search(output_regexp, srun.stdout, re.ASCII)

            if match is None:
                e = "srun output '{:s}' does not match expected format"
                raise ValueError(e.format(srun.stdout))

            version_major = int(match.group(1))
            # the minor version might be of the form `05` but the Python int()
            # function handles this correctly
            version_minor = int(match.group(2))
            version_patch = int(match.group(3))

            if version_major < 19 or (
                version_major == 19 and version_minor < 5
            ):
                warnings.warn(
                    "Melissa has not been tested with Slurm versions older than 19.05.5",
                    RuntimeWarning
                )

            if version_major < 17 or (
                version_major == 17 and version_minor < 11
            ):
                raise RuntimeError(
                    "expected at least Slurm 17.11, got {:s}.{:s}.{:s} which does not support heterogeneous jobs"
                    .format(*match.groups())
                )

            self.use_het_prefix = version_major >= 20

    def sanity_check_impl(self, options: Options) -> List[str]:
        args = options.raw_arguments
        errors = []

        for a in args:
            if a[0] != "-":
                errors.append("non-option argument `{:s}` detected".format(a))
            elif a in ["-n", "--ntasks", "--test-only"]:
                errors.append("remove `{:s}` argument".format(a))

        command = ["srun", "--test-only", "--ntasks=1"] + args + ["--", "true"]
        srun = subprocess.run(
            command, stderr=subprocess.PIPE, universal_newlines=True
        )

        if srun.returncode != 0:
            e = "srun error on trial execution: {:s}".format(srun.stderr)
            errors.append(e)

        return errors

    def submit_heterogeneous_job_impl(
        self, commands: List[List[str]], environment: Dict[str, str], name: str,
        options: Options
    ) -> SlurmJob:
        sbatch_env = os.environ.copy()
        sbatch_env.update(environment)

        num_commands = len(commands)
        num_tasks = options.num_processes
        output_filename = "{:s}.%J.stdout".format(name)
        error_filename = "{:s}.%J.stderr".format(name)

        # sbatch Script and srun Arguments Assembly
        #
        # Command arguments are assembled initially as lists of lists of
        # strings (`List[List[str]]`) to avoid ambiguities, to allow sanity
        # checks, and to allow sanitizing the text later. The inner lists are
        # * lines in sbatch scripts
        # * hetgroup arguments on the srun command line.
        #
        # The actual strings are created in two steps:
        # * The inner lists are turned into strings with `#SBATCH`, `:`
        #   prefixes, back-slashes, and so on *without* newlines at the end.
        # * The list of strings is turned into a string and the missing
        #   newlines are added.
        #
        # The convention of leaving away newlines means that an empty string
        # will be replaced a newline.

        # assemble sbatch options
        #
        # the options below need to be specified only once; these are so-called
        # "propagated options" in the Slurm documentation
        # user-provided options are assumed to be non-propagated
        propagated_options = [
            ["--output={:s}".format(output_filename)],
            ["--error={:s}".format(error_filename)]
        ]

        hetjob_keyword = "hetjob" if self.use_het_prefix else "packjob"
        hetjob_options = [] # type: List[List[str]]
        for i, _ in enumerate(commands):
            job_options = options.raw_arguments + [
                "--ntasks={:d}".format(num_tasks)
            ]
            hetjob_options.append(job_options)
            if i + 1 < len(commands):
                hetjob_options.append([hetjob_keyword])

        sbatch_options = propagated_options + hetjob_options

        # serialize sbatch options
        def options2str(options: List[str]) -> str:
            options_str = " ".join(options)
            return "#SBATCH " + options_str

        sbatch_options_str = [options2str(o) for o in sbatch_options]

        print(sbatch_options_str)

        # assemble srun arguments
        hetgroup_keyword = "het-group" if self.use_het_prefix else "pack-group"

        srun_arguments = [] # type: List[List[str]]
        if len(commands) == 1:
            srun_arguments = [["--"] + commands[0]]
        else:
            for i, cmd in enumerate(commands):
                args = ["--{:s}={:d}".format(hetgroup_keyword, i), "--"] + cmd
                srun_arguments.append(args)

        # serialize srun arguments
        def args2str(hetgroup: int, args: List[str]) -> str:
            assert hetgroup >= 0
            assert hetgroup < len(commands)

            prefix = ": " if hetgroup > 0 else ""
            suffix = " \\" if hetgroup + 1 < len(commands) else ""
            return "  " + prefix + " ".join(args) + suffix

        srun_arguments_str = [
            args2str(i, args) for i, args in enumerate(srun_arguments)
        ]

        # write srun calls to file
        sbatch_script_filename = "sbatch.sh"
        sbatch_script = (
            ["#!/bin/sh"] + ["# sbatch script for job {:s}".format(name)] +
            sbatch_options_str + [""] + ["srun \\"] + srun_arguments_str
        )
        # POSIX requires files to end with a newline; missing newlines at the
        # end of a file may break scripts that append text.
        # this string won't contain a newline at the end; it must be added
        # manually or by using a function that adds it, e.g., `print`
        sbatch_script_str_noeol = "\n".join(sbatch_script)

        with open(sbatch_script_filename, "w") as f:
            print(sbatch_script_str_noeol, file=f, flush=True)

        sbatch_call = \
            ["sbatch"] \
            + ["--parsable"] \
            + ["--job-name={:s}".format(name)] \
            + [sbatch_script_filename]

        sbatch = subprocess.run(
            sbatch_call,
            env=sbatch_env,
            stdin=subprocess.DEVNULL,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
            check=True
        )

        if sbatch.stderr != "":
            print("sbatch: {:s}".format(sbatch.stderr), file=sys.stderr)

        match = self.sbatch_job_id_pattern.fullmatch(sbatch.stdout.strip())

        if match is None:
            e = "no job ID found in sbatch output: '{:s}'"
            raise ValueError(e.format(sbatch.stdout))

        return SlurmJob(int(match.group(1)))

    def cancel_jobs_impl(self, jobs: List[SlurmJob]) -> None:
        job_list = ["--job={:d}".format(j.id()) for j in jobs]
        scancel_command = ["scancel", "--batch", "--quiet"] + job_list
        scancel = subprocess.run(
            scancel_command,
            stdin=subprocess.DEVNULL,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
            check=True
        )

    def update_jobs_impl(self, jobs: List[SlurmJob]) -> None:
        job_list = ["--job={:d}".format(j.id()) for j in jobs]
        sacct_command = \
            ["sacct", "--parsable2", "--format=JobID,State"] \
            + job_list
        sacct = subprocess.run(
            sacct_command,
            stdin=subprocess.DEVNULL,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
            check=True
        )

        job_map = dict([(j.id(), j) for j in jobs])

        for line in sacct.stdout.strip().split("\n"):
            self.update_jobs_impl_pure(job_map, line)

    def update_jobs_impl_pure(
        self, job_map: Dict[int, SlurmJob], sacct_line: str
    ) -> None:
        slurm_states_waiting = ["PENDING", "REQUEUED", "SUSPENDED", "RESIZING"]
        slurm_states_failure = [
            "BOOT_FAIL", "CANCELLED", "DEADLINE", "FAILED", "NODE_FAIL",
            "OUT_OF_MEMORY", "PREEMPTED", "TIMEOUT"
        ]

        match = self.sacct_line_pattern.fullmatch(sacct_line)

        if match is None:
            return

        job_id = int(match.group(1))
        slurm_state = match.group(3)

        assert job_id in job_map

        j = job_map[job_id]

        if slurm_state == "REVOKED":
            e = "cannot handle Slurm siblings like job {:d}"
            raise NotImplementedError(e.format(job_id))

        if slurm_state in slurm_states_waiting:
            assert j.state() in [State.WAITING, State.RUNNING]
            j.set_state(State.WAITING)
        elif slurm_state in ["RUNNING"]:
            assert j.state() in [State.WAITING, State.RUNNING]
            j.set_state(State.RUNNING)
        elif slurm_state in ["COMPLETED"]:
            assert j.state() in [State.WAITING, State.RUNNING, State.TERMINATED]
            j.set_state(State.TERMINATED)
        elif slurm_state in slurm_states_failure:
            assert j.state() in [State.WAITING, State.RUNNING, State.FAILED]
            j.set_state(State.FAILED)
        else:
            e = "unknown Slurm job state '{:s}'"
            raise RuntimeError(e.format(slurm_state))


class TestSlurmScheduler(unittest.TestCase):
    @unittest.skipIf(shutil.which("srun") is None, "srun executable not found")
    def test_Slurm_version(self):
        try:
            s = Slurm()
        except Exception as e:
            print(e)

    def test_update_jobs_impl_pure(self):
        jobs = [SlurmJob(251), SlurmJob(7)]
        job_map = dict([(j.id(), j) for j in jobs])
        scheduler = Slurm(testing=True)

        scheduler.update_jobs_impl_pure(job_map, "nonsense")
        self.assertEqual(jobs[0].state(), State.WAITING)
        self.assertEqual(jobs[1].state(), State.WAITING)

        scheduler.update_jobs_impl_pure(job_map, "251|PENDING")
        self.assertEqual(jobs[0].state(), State.WAITING)
        self.assertEqual(jobs[1].state(), State.WAITING)

        scheduler.update_jobs_impl_pure(job_map, "7|RUNNING")
        self.assertEqual(jobs[0].state(), State.WAITING)
        self.assertEqual(jobs[1].state(), State.RUNNING)

        scheduler.update_jobs_impl_pure(job_map, "7|FAILED")
        self.assertEqual(jobs[0].state(), State.WAITING)
        self.assertEqual(jobs[1].state(), State.FAILED)

        scheduler.update_jobs_impl_pure(job_map, "251|RUNNING")
        self.assertEqual(jobs[0].state(), State.RUNNING)
        self.assertEqual(jobs[1].state(), State.FAILED)

        scheduler.update_jobs_impl_pure(job_map, "251|COMPLETED")
        self.assertEqual(jobs[0].state(), State.TERMINATED)
        self.assertEqual(jobs[1].state(), State.FAILED)

        scheduler.update_jobs_impl_pure(job_map, "7|FAILED")
        self.assertEqual(jobs[0].state(), State.TERMINATED)
        self.assertEqual(jobs[1].state(), State.FAILED)


if __name__ == "__main__":
    unittest.main()

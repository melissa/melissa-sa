#!/usr/bin/python3

# Copyright (c) 2020, Institut National de Recherche en Informatique et en Automatique (Inria)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


# This program redirects standard output and standard error to files. The
# filename is composed of
# * executable name,
# * timestamp (UNIX epoch), and
# * the process ID.


import math
import os
import shutil
import sys
import time


def replace_filedescriptor(filename, f):
    assert isinstance(filename, str)

    fd = f.fileno()

    try:
        flags = os.O_WRONLY | os.O_CREAT | os.O_TRUNC
        mode = 0o644
        fd_new = os.open(filename, flags, mode)
        os.dup2(fd_new, fd)
    finally:
        os.close(fd_new)



def main():
    if len(sys.argv) < 2:
        return

    name = os.path.basename(sys.argv[1])
    pid = os.getpid()
    now_sec = math.floor(time.time())
    filename_fmt = "{:s}.{:d}.{:d}.{{:s}}".format(name, now_sec, pid)

    replace_filedescriptor(filename_fmt.format("out"), sys.stdout)
    replace_filedescriptor(filename_fmt.format("err"), sys.stderr)

    have_stdbuf = shutil.which("stdbuf") is not None

    if have_stdbuf:
        cmdline = ["stdbuf", "--output=L", "--"] + sys.argv[1:]
    else:
        cmdline = sys.argv[1:]

    os.execvp(cmdline[0], cmdline)


if __name__ == "__main__":
    sys.exit(main())
